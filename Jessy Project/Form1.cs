﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using Bangle.Comman;
using Bangle.BLL;

namespace Jessy_Project
{
    public partial class Form1 : Form
    {
        T_CompanyBLL bll = new T_CompanyBLL();


        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string _sHtmlString = HttpHeler.GetHtml("http://www.51job.com/");

            HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
            document.LoadHtml(_sHtmlString);

            HtmlNode rootNode = document.DocumentNode;
            HtmlNodeCollection categoryNodeList = rootNode.SelectNodes("//html[1]/body[1]//a");

            StringBuilder sMsg = new StringBuilder();
            foreach (HtmlNode item in categoryNodeList)
            {
                string _sHref = item.GetAttributeValue("href", "");
                string _sTitle = item.GetAttributeValue("title", "");
                string _sAdid = item.GetAttributeValue("adid", "");

                string _sBaseUrl = "http://companyadc.51job.com/";
                if (_sHref.Length > _sBaseUrl.Length && _sHref.Substring(0, _sBaseUrl.Length) == _sBaseUrl)
                {


                    if (bll.CheckByADUrl(_sAdid) <= 0)
                    {
                        bll.Insert("51Job首页", _sBaseUrl, _sAdid, _sTitle, DateTime.Now, _sHref, "", "", null, 0);
                        sMsg.Append(_sHref + "   ,  " + _sTitle + "   ,  " + _sAdid + "\r\n");
                    }
                }
            }

            this.textBox1.Text = sMsg.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string _url = HttpHeler.GetRedireUrl("http://ac.51job.com/phpAD/adtrace.php?ID=17356219");
            string _sHtmlString = HttpHeler.GetHtml(_url);
            this.textBox1.Text = _sHtmlString;

        }
    }
}
