﻿using Bangle.BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace Query
{
    public partial class Form1 : Form
    {
        T_CompanyBLL bll = new T_CompanyBLL();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            this.ddl_TitleType.ComboBox.Items.Add("今天");
            this.ddl_TitleType.ComboBox.Items.Add("7天内的");
            this.ddl_TitleType.ComboBox.Items.Add("30天内的");
            this.ddl_TitleType.ComboBox.SelectedIndex = 0;

            this.ddl_TitleType.SelectedIndexChanged += ddl_TitleType_SelectedIndexChanged;
            Query();

        }

        private void btn_Query_Click(object sender, EventArgs e)
        {
            Query();
        }

        private void ddl_TitleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Query();
        }

        private void txt_Query_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                Query();
            }
        }


        private void cb_Souce_SelectedIndexChanged(object sender, EventArgs e)
        {
            Query();
        }


        #region 共用方法
        private void Query()
        {
            DateTime QTime = DateTime.MinValue;
            switch (this.ddl_TitleType.SelectedIndex)
            {
                case 0:
                    QTime = DateTime.Today;
                    break;
                case 1:
                    QTime = DateTime.Today.AddDays(-7);
                    break;
                case 2:
                    QTime = DateTime.Today.AddDays(-30);
                    break;
            }

            DataTable dt = bll.YQuery(QTime, sQTitle);
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.DataSource = dt;

            this.lbl_SysMsg.Text = string.Format("共 {0} 条数据", dt.Rows.Count);
        }
        #endregion


        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 0)
            {
                string sUrl = dataGridView1.Rows[e.RowIndex].Cells["vUrl"].Value.ToString();
                if (sUrl.Length < 10)
                {
                    return;
                }
                System.Diagnostics.Process.Start("iexplore.exe", sUrl);
            }
        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            //this.dataGridView1.BeginEdit(true);
        }


    }
}
