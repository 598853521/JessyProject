﻿namespace Query
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lbl_SysMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ddl_TitleType = new System.Windows.Forms.ToolStripComboBox();
            this.txt_Query = new System.Windows.Forms.ToolStripTextBox();
            this.btn_Query = new System.Windows.Forms.ToolStripLabel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.OpenWeb = new System.Windows.Forms.DataGridViewImageColumn();
            this.vUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RecordTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CompanyUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cb_Souce = new System.Windows.Forms.ToolStripComboBox();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl_SysMsg,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 311);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(573, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lbl_SysMsg
            // 
            this.lbl_SysMsg.Name = "lbl_SysMsg";
            this.lbl_SysMsg.Size = new System.Drawing.Size(16, 17);
            this.lbl_SysMsg.Text = "  ";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(476, 17);
            this.toolStripStatusLabel2.Spring = true;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(66, 17);
            this.toolStripStatusLabel1.Text = "By Bangle";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ddl_TitleType,
            this.cb_Souce,
            this.txt_Query,
            this.btn_Query});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(573, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // ddl_TitleType
            // 
            this.ddl_TitleType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddl_TitleType.Name = "ddl_TitleType";
            this.ddl_TitleType.Size = new System.Drawing.Size(75, 25);
            // 
            // txt_Query
            // 
            this.txt_Query.Name = "txt_Query";
            this.txt_Query.Size = new System.Drawing.Size(300, 25);
            this.txt_Query.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_Query_KeyPress);
            // 
            // btn_Query
            // 
            this.btn_Query.Image = ((System.Drawing.Image)(resources.GetObject("btn_Query.Image")));
            this.btn_Query.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btn_Query.Name = "btn_Query";
            this.btn_Query.Size = new System.Drawing.Size(48, 17);
            this.btn_Query.Text = "查询";
            this.btn_Query.Click += new System.EventHandler(this.btn_Query_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.OpenWeb,
            this.vUrl,
            this.Source,
            this.CompanyName,
            this.RecordTime,
            this.CompanyUrl});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 25);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(573, 286);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentDoubleClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // OpenWeb
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.OpenWeb.DefaultCellStyle = dataGridViewCellStyle7;
            this.OpenWeb.HeaderText = "";
            this.OpenWeb.Image = global::Query.Properties.Resources.world_link;
            this.OpenWeb.Name = "OpenWeb";
            this.OpenWeb.ReadOnly = true;
            this.OpenWeb.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.OpenWeb.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.OpenWeb.ToolTipText = "打开网站";
            this.OpenWeb.Width = 40;
            // 
            // vUrl
            // 
            this.vUrl.DataPropertyName = "YUrl";
            this.vUrl.HeaderText = "vUrl";
            this.vUrl.Name = "vUrl";
            this.vUrl.Visible = false;
            // 
            // Source
            // 
            this.Source.DataPropertyName = "Source";
            this.Source.HeaderText = "来源";
            this.Source.Name = "Source";
            this.Source.ReadOnly = true;
            // 
            // CompanyName
            // 
            this.CompanyName.DataPropertyName = "ADTitle";
            this.CompanyName.HeaderText = "公司名称";
            this.CompanyName.Name = "CompanyName";
            this.CompanyName.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.CompanyName.Width = 300;
            // 
            // RecordTime
            // 
            this.RecordTime.DataPropertyName = "ADTime";
            dataGridViewCellStyle8.Format = "yyyy-MM-dd HH:mm:ss";
            this.RecordTime.DefaultCellStyle = dataGridViewCellStyle8;
            this.RecordTime.HeaderText = "收录时间";
            this.RecordTime.Name = "RecordTime";
            this.RecordTime.ReadOnly = true;
            this.RecordTime.Width = 120;
            // 
            // CompanyUrl
            // 
            this.CompanyUrl.DataPropertyName = "YUrl";
            this.CompanyUrl.HeaderText = "网址";
            this.CompanyUrl.Name = "CompanyUrl";
            this.CompanyUrl.Width = 300;
            // 
            // cb_Souce
            // 
            this.cb_Souce.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cb_Souce.Name = "cb_Souce";
            this.cb_Souce.Size = new System.Drawing.Size(121, 25);
            this.cb_Souce.Visible = false;
            this.cb_Souce.SelectedIndexChanged += new System.EventHandler(this.cb_Souce_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 333);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Query";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripComboBox ddl_TitleType;
        private System.Windows.Forms.ToolStripTextBox txt_Query;
        private System.Windows.Forms.ToolStripLabel btn_Query;
        private System.Windows.Forms.ToolStripStatusLabel lbl_SysMsg;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.DataGridViewImageColumn OpenWeb;
        private System.Windows.Forms.DataGridViewTextBoxColumn vUrl;
        private System.Windows.Forms.DataGridViewTextBoxColumn Source;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyName;
        private System.Windows.Forms.DataGridViewTextBoxColumn RecordTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn CompanyUrl;
        private System.Windows.Forms.ToolStripComboBox cb_Souce;
    }
}

