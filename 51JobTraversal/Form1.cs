﻿using Bangle.BLL;
using Bangle.Comman;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _51JobTraversal
{
    public partial class Form1 : Form
    {
        #region 属性
        T_CompanyBLL bll = new T_CompanyBLL();
        #endregion

        public Form1()
        {
            InitializeComponent();
        }
        int iADId = 17356290;

        private void btn_Run_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1000 * 10;
            timer1.Enabled = !timer1.Enabled;
            this.btn_Run.Text = timer1.Enabled ? "Stop" : "Run";
        }

        private int GetDateByWeb()
        {
            //string sADCode = "17356219";
            string sADCode = iADId.ToString();


            if (bll.CheckByADUrl(sADCode) > 0)
            {
                this.label1.Text = string.Format("【{0}】 已存在", sADCode);
                return 1;
            }

            string sADUrl = "http://ac.51job.com/phpAD/adtrace.php?ID=" + sADCode;
            string sYUrl = HttpHeler.GetRedireUrl(sADUrl);

            if (sYUrl == "")
            {
                this.label1.Text = string.Format("【{0}】 无效ID", sADCode);
                return 3;
            }
            else if (sYUrl != sADUrl)
            {
                string sHtmlString = HttpHeler.GetHtml(sYUrl);

                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(sHtmlString);

                HtmlNode rootNode = document.DocumentNode;

                string sTitle = "";
                string sDescription = "";

                #region Title
                {
                    HtmlNodeCollection categoryNodeList = rootNode.SelectNodes("//html[1]/head[1]/title[1]");
                    if (categoryNodeList != null)
                    {
                        foreach (HtmlNode item in categoryNodeList)
                        {
                            sTitle = item.InnerText;
                        }
                    }
                }
                #endregion

                #region Description
                {
                    HtmlNodeCollection categoryNodeList = rootNode.SelectNodes("//html[1]/head[1]/meta");
                    if (categoryNodeList != null)
                    {
                        foreach (HtmlNode item in categoryNodeList)
                        {
                            string _sName = item.GetAttributeValue("name", "");
                            if (_sName.ToUpper() == "DESCRIPTION")
                            {
                                sDescription = item.GetAttributeValue("content", "");
                            }
                        }
                    }
                }
                #endregion

                //【海澜集团有限公司招聘，求职】海澜集团有限公司前程无忧官方招聘网站
                string sADTitle = (sTitle.IndexOf("招聘，求职】") > 0 ? sTitle.Substring(0, sTitle.IndexOf("招聘，求职】")).Replace("【", "") : sTitle);
                bll.Insert("51Job自动搜索", sADUrl, sADCode, sADTitle, DateTime.Now, sYUrl, sTitle, sDescription, DateTime.Now, 0);
                this.label1.Text = string.Format("【{0}】 保存成功", sADCode);

                return 60;
            }
            else
            {
                this.label1.Text = string.Format("【{0}】 其它", sADCode);
                return 1;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                timer1.Interval = 1000 * GetDateByWeb();
            }
            catch (Exception ex)
            {
                this.textBox1.Text = this.textBox1.Text + "\r\n\r\n\r\nEX:" + ex.ToString();
            }
            finally
            {
                iADId++;
            }
        }
    }
}


//EX:System.ArgumentOutOfRangeException: 长度不能小于 0。
//参数名: length
//   在 System.String.InternalSubStringWithChecks(Int32 startIndex, Int32 length, Boolean fAlwaysCopy)
//   在 System.String.Substring(Int32 startIndex, Int32 length)
//   在 _51JobTraversal.Form1.GetDateByWeb() 位置 g:\wqy\VS2013\Test\Jessy Project\51JobTraversal\Form1.cs:行号 97
//   在 _51JobTraversal.Form1.timer1_Tick(Object sender, EventArgs e) 位置 g:\wqy\VS2013\Test\Jessy Project\51JobTraversal\Form1.cs:行号 114