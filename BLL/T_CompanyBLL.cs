﻿using Bangle.BLL.DataSet1TableAdapters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace Bangle.BLL
{
    public class T_CompanyBLL
    {
        T_CompanyTableAdapter company_bll = new T_CompanyTableAdapter();

        public DataTable GetData()
        {
            return company_bll.GetData().CopyToDataTable();
        }

        public int Insert(string Source, string SourceUrl, string ADUrl, string ADTitle, DateTime? ADTime, string YUrl, string YTitle, string YDescription, DateTime? YTime, int OldID)
        {
            return company_bll.Insert(ADUrl, ADTitle, ADTime, YUrl, YTitle, YDescription, YTime, OldID, Source, SourceUrl);
        }

        public int CheckByADUrl(string ADUrl)
        {
            return Convert.ToInt32(company_bll.CheckByADUrl(ADUrl));
        }

        public DataTable YQuery(DateTime BeginTime, string Title)
        {
            DataSet1.T_CompanyDataTable c_dt = company_bll.YQuery(BeginTime, Title);
            DataTable dt = null;
            if (c_dt != null && c_dt.Count() > 0)
            {
                dt = company_bll.YQuery(BeginTime, Title).DefaultIfEmpty().CopyToDataTable();
            }
            else
            {
                dt = new DataTable();
            }
            return dt;
        }
    }
}
